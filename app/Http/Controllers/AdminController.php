<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Category;
use App\News;

class AdminController extends Controller
{

    public function login(Request $request)
    {
    	if($request->isMethod('post'))
    	{
    		$data = $request->input();
    		if(Auth::attempt(['email' => $data['email'],'password' => $data['password'],'admin' =>'1'])){
    			return redirect('/admin/addCategory');
    		}
    		else{
    		   return redirect('/admin')->with('flash_message_error','invalid email or password');
    		}
    	}
    	return view('admin.admin_login');
    }

    public function dashboard()
    {
    	//return view('admin.admin_dashboard');
    }
    
    public function category()
    {
    	return view('admin.add_category');
    }
    public function addCategory(Request $request)
    {
    	$this->validate($request,[
         'categoryImage'  => 'nullable|image|mimes:jpg,jpeg,png|max:2048'
        ]);
        if($request->hasFile('categoryImage'))
        {
            $fileObject      = $request->file('categoryImage');
            $extension       = $fileObject->getClientOriginalExtension();
            $mimeType        = $fileObject->getClientMimeType();
            $fileName        = $fileObject->getClientOriginalName();
            $size            = $fileObject->getClientSize();
            $photoPath       = $fileObject->storeAs('category imgs',$request->input('categoryName').'.'.$extension);
            $cat_image       = $request->input('categoryName').'.'.$extension; 
        }
        $cat = new Category();
        $cat->title = $request->input('categoryName');
        $cat->image = $cat_image;
        $cat->save();
    	return redirect()->route('category')->with('success','Category added successfully');
    }

    public function news()
    {
    	$category = Category::all();
        return view('admin.add_news')->with('category',$category);	
    }

    public function addNews(Request $request)
    {
    	$this->validate($request,[
         'chooseCategory' => 'required',
         'details'        => 'required',
         'newsImage'      => 'nullable|image|mimes:jpg,jpeg,png|max:2048'
        ]);
        if($request->hasFile('newsImage'))
        {
            $fileObject      = $request->file('newsImage');
            $extension       = $fileObject->getClientOriginalExtension();
            $mimeType        = $fileObject->getClientMimeType();
            $fileName        = $fileObject->getClientOriginalName();
            $size            = $fileObject->getClientSize();
            $photoPath       = $fileObject->storeAs('news imgs',$request->input('newsTitle').'.'.$extension);
            $news_image      = $request->input('newsTitle').'.'.$extension; 
        }
        $news = new News();
        $news->category = $request->input('chooseCategory');
        $news->title    = $request->input('newsTitle');
        $news->image    = $news_image;
        $news->details  = strip_tags($request->input('details'));
        $news->save();
    	return redirect()->route('news')->with('success','News added successfully');

    }

    public function logout()
    {
    	session::flush();
    	return redirect('/admin')->with('flash_message_success','logged out successfully');
    }
}
