<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\News;
class UiController extends Controller
{
    public function index()
	{
		$categories = Category::all();
        return view('frontend.usrInterface')->with('categories',$categories);
	}

	public function news($category)
	{
		$categories = Category::all();
		$news       = News::where('category', '=',$category)->get();
		return view('frontend.news')->with('categories',$categories)->with('news',$news);
	}

	public function newsdetails($id)
	{
		$categories = Category::all();
		$news       = News::find($id);
		return view('frontend.newsDetails')->with('categories',$categories)->with('news',$news);
	}
}
