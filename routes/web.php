<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/news','UiController@index');
Route::get('/news/{category}', 'UiController@news')->name('UInews');
Route::get('/news/details/{id}', 'UiController@newsdetails')->name('newsdetails');

////////////////admin route///////////////
Route::match(['get','post'],'/admin', 'AdminController@login');
//Route::get('/admin/dashboard', 'AdminController@dashboard');
Route::get('/admin/addCategory', 'AdminController@category')->name('category');
Route::post('/admin/addCategory', 'AdminController@addCategory')->name('addCategory');
Route::get('/admin/addNews', 'AdminController@news')->name('news');
Route::post('/admin/addNews', 'AdminController@addNews')->name('addNews');
Route::get('/logout', 'AdminController@logout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
