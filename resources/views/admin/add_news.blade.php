<!DOCTYPE html>
<html lang="en">
<head>
<title>Matrix Admin</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="{{asset('css/backend_css/bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/backend_css/bootstrap-responsive.min.css')}}" />
<link rel="stylesheet" href="{{asset('css/backend_css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('css/backend_css/select2.css')}}" />
<link rel="stylesheet" href="{{asset('css/backend_css/matrix-style.css')}}" />
<link rel="stylesheet" href="{{asset('css/backend_css/matrix-media.css')}}" />
<link href="{{asset('fonts/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>
@include('layouts.admin_header')
@include('layouts.admin_sidebar')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{route('addNews')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Categories</a> <a href="#" class="current">Add News</a> </div>
    <h1>Add News</h1>
  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        @if (count($errors) > 0)
           <div class="alert alert-danger alert-dismissible" role="alert">
            @foreach($errors->all() as $error)
              <strong>{{$error}}</strong> .
            @endforeach  
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        @endif
        @if($message = Session::get('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <strong>{{$message}}</strong>.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Add News</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="{{route('addNews')}}" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="control-group">
                <label class="control-label">Choose Category</label>
                <div class="controls">
                  <select name="chooseCategory" class="form-control">
                    <option ></option>
                    @if(isset($category) && count($category) > 0)
                    @foreach($category as $cat)
                    <option value="{{$cat->title}}" @if(old('chooseCategory') == $cat->title) selected @endif>{{$cat->title}}</option>
                    @endforeach
                    @endif
                  </select>
                </div>
              </div>
             
              <div class="control-group">
                <label class="control-label">News Title</label>
                <div class="controls">
                  <input type="text" name="newsTitle"  required @if (count($errors) > 0) value="{{old('newsTitle')}}" @endif>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">News Image</label>
                <div class="controls">
                  <input type="file" name="newsImage" required  @if (count($errors) > 0) value="{{old('newsImage')}}" @endif>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">News Details</label>
                <div class="controls">
                  <div class="content-box-large">
                    <div class="panel-heading">
                    <div class="panel-title">CKEditor Full</div>
                    
                    <div class="panel-options">
                      <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                      <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                    </div>
                  </div>
                    <div class="panel-body">
                      <textarea id="ckeditor_full" name="details">
                        @if (count($errors) > 0){{old('details')}} @endif
                      </textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-actions">
                <input type="submit" value="Save" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('layouts.admin_footer')
<link rel="stylesheet" type="text/css" href="{{asset('vendors/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css')}}"></link>
<script src="{{asset('js/backend_js/jquery.min.js')}}"></script> 
<script src="{{asset('js/backend_js/jquery.ui.custom.js')}}"></script> 
<script src="{{asset('js/backend_js/bootstrap.min.js')}}"></script> 
<script src="{{asset('js/backend_js/jquery.uniform.js')}}"></script> 
<script src="{{asset('js/backend_js/select2.min.js')}}"></script> 
<script src="{{asset('js/backend_js/jquery.validate.js')}}"></script> 
<script src="{{asset('js/backend_js/matrix.js')}}"></script> 
<script src="{{asset('js/backend_js/matrix.form_validation.js')}}"></script>
<script src="{{asset('vendors/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.js')}}"></script>
<script src="{{asset('vendors/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js')}}"></script>
<script type="text/javascript" src="{{asset('vendors/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script src="{{asset('vendors/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('vendors/ckeditor/adapters/jquery.js')}}"></script>
<script src="{{asset('js/backend_js/editors.js')}}"></script>
</body>
</html>
