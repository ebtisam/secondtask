<!DOCTYPE html>
<html>
<head>
	<title>UI</title>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="{{asset('css/frontend_css/bootstrap.min.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/frontend_css/owl.carousel.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/frontend_css/owl.theme.default.min.css')}}"/>
    <style type="text/css">
    	.cat{
    		margin: 15px 15px;
    	}
        .card:hover{
        	background: rgba(0,0,0,.8);
        }
        .new{
        	margin:50px 0; 
        }
        .mar{
        	margin-bottom: 20px;
        	margin-top: 20px;
        }
    </style>
</head>
<body>
@include('frontend.cat')
<div class="newsdetails">
	<div class="container">
		<div><h1>News Details</h1></div>
		@if(isset($news) )
		<div class="text-center new">
			<h3>{{$news->title}}</h3>
			<img src="/storage/news imgs/{{$news->image}}" class=" mar">
			<p>{{$news->details}}</p>
		</div>
		@endif
	</div>
</div>

<script src="{{asset('js/frontend_js/jquery.min.js')}}"></script>
<script src="{{asset('js/frontend_js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/frontend_js/owl.carousel.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.owl-carousel').owlCarousel({
		    loop:true,
		    margin:10,
		    nav:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:3
		        },
		        1000:{
		            items:4
		        }
		    }
		});
		var owl = $('.owl-carousel');
			owl.owlCarousel({
			    items:4,
			    loop:true,
			    margin:10,
			    autoplay:true,
			    autoplayTimeout:1000,
			    autoplayHoverPause:true
			});
			 owl.trigger('play.owl.autoplay',[3000]);

	});
</script>
</body>
</html>