	<div class="categories">
		<div class="container">
			<div class="text-center cat"><h1>Categories</h1></div>
			<div class="owl-carousel owl-theme">
				@if(isset($categories) && count($categories) > 0)
				@foreach($categories as $cat)
				  <a href="{{route('UInews',$cat->title)}}">
				  	<div class="card" style="height: 250px; width: 100%;">
				    <img class="card-img-top" style="height: 180px; width: 100%;" 
				    src="/storage/category imgs/{{$cat->image}}" alt="Card image cap">
				    <div class="card-body">
				      <h5 class="card-title text-center">{{$cat->title}}</h5>
				    </div>
				  </div>
				  </a>
				    
				@endforeach
				@endif
			  
			</div>
		</div>
	</div>