<!DOCTYPE html>
<html>
<head>
	<title>UI</title>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="{{asset('css/frontend_css/bootstrap.min.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/frontend_css/owl.carousel.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/frontend_css/owl.theme.default.min.css')}}"/>
    <style type="text/css">
    	.cat{
    		margin: 15px 15px;
    	}
        .card:hover{
        	background: rgba(0,0,0,.8);
        }
        .new{
        	margin:50px 0; 
        }
        .pad{
        	margin-bottom: 50px;
        }
    </style>
</head>
<body>
@include('frontend.cat')
<div class="news">
	<div class="container">
		<div class="text-center new"><h1>News</h1></div>
		
		@if(isset($news) && count($news) > 0)
		@foreach($news as $news)
		<div class="row pad">
	      	<div class="col-md-4">
	      		<div class="card" style="width: 18rem;">
				  <img class="card-img-top" style="width: 100%;height: 150px" src="/storage/news imgs/{{$news->image}}" alt="Card image cap">
				</div>
	      	</div>
	      	<div class="col-md-8">
	      		<h3>{{$news->title}}</h3>
	      		<p>{{ substr($news->details, 0,50) }}</p>
	      		<a href="{{route('newsdetails',$news->id)}}" class="btn btn-info">Read More...</a>
	      	</div>	
        </div>
	    @endforeach
	    @else
	    <div class="alert alert-warning alert-dismissible fade show" role="alert">
		  <strong>sorry!</strong> this category don't have any news now.
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		  </button>
		</div>		    
	    @endif		    
		
			    
	</div>
</div>

<script src="{{asset('js/frontend_js/jquery.min.js')}}"></script>
<script src="{{asset('js/frontend_js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/frontend_js/owl.carousel.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.owl-carousel').owlCarousel({
		    loop:true,
		    margin:10,
		    nav:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:3
		        },
		        1000:{
		            items:4
		        }
		    }
		});
		var owl = $('.owl-carousel');
			owl.owlCarousel({
			    items:4,
			    loop:true,
			    margin:10,
			    autoplay:true,
			    autoplayTimeout:1000,
			    autoplayHoverPause:true
			});
			 owl.trigger('play.owl.autoplay',[3000]);

	});
</script>
</body>
</html>